from tortoise import fields
from tortoise.models import Model


class CargoType(Model):
    class Meta:
        table = 'cargo_type'

    id = fields.IntField(pk=True)
    date = fields.DateField()
    cargo_type = fields.CharField(max_length=100)
    rate = fields.DecimalField(max_digits=10, decimal_places=3)
