from models import CargoType
import json
from dotenv import load_dotenv
import os
from fastapi import FastAPI
import uvicorn
from tortoise.contrib.fastapi import register_tortoise
from tortoise.exceptions import DoesNotExist


app = FastAPI(debug=True)
load_dotenv('./docker-compose/.env')
def get_env(arr: list) -> tuple:
    match arr:
        case list():
            envs = []
            for env in arr:
                envs.append(os.getenv(env, {}))
            return tuple(envs)
        case _:
            return TypeError(f'Invalid type. Excpected: list, received: {type(arr).__name__}')


db_credentials = get_env(['POSTGRES_USER', 'POSTGRES_PASSWORD', 'POSTGRES_PORT', 'POSTGRES_DB'])
dbuser, dbpass, dbport, dbname = db_credentials
db_container_name = 'db_insurance'
# db_container_name = 'localhost'
db_url = f"postgres://{dbuser}:{dbpass}@{db_container_name}:{dbport}/{dbname}"
print(db_url)

register_tortoise(
    app=app,
    db_url=db_url,
    modules={'models': ['models']},
    generate_schemas=True,
    add_exception_handlers=True,
)

ACTUAL_PRICE = 10_000
@app.get('/calculate_insurance')
async def calculate_insurance(date: str, cargo_type: str) -> dict:
    cargo_type_rate = await CargoType.filter(cargo_type=cargo_type, date=date).first()
    if cargo_type_rate:
        total_price = ACTUAL_PRICE * float(cargo_type_rate.rate)
        total_price = f'{total_price:.2f}'
        return {
            'type': cargo_type,
            'total_price': total_price,
        }
    return {'err': 'Cargo type not found'}


async def load_rates():
    with open('insurance_rate.json', 'r') as json_file:
        rates = json.load(json_file)
        for date, data in rates.items():
            for rate in data:
                await CargoType.get_or_create(date=date, cargo_type=rate['cargo_type'], rate=rate['rate'])

@app.on_event('startup')
async def startup():
    await load_rates()


if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=8001)